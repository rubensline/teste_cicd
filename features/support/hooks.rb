
## Cucumber Definitions

Before do

  ## configure the chosen browser

  Capybara.configure do |config|

    config.default_driver = :selenium_chrome_headless

  end

  ## set default max wait and maximize browser

end